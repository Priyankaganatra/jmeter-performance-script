#!/bin/sh
_baseDir="/media/home/home/qatester/Desktop/Quicken/jmeter-performance-script"

############################################
_reportDataDir="$_baseDir/report_data"
_reportsDir="$_baseDir/reports"
_now=$(date +"%Y_%m_%d-%H_%M_%S")
_newReportDir="$_reportsDir/report_$_now/"
_reportFile="report_$_now"

# rm -rf ../reports/html/
echo "Creating directory: $_reportsDir"
mkdir "$_reportsDir" > /dev/null 2>&1
jmeter -g "$_reportDataDir/csv/report.csv" -o "$_newReportDir" -Dlog4j.logger.org.apache.commons.httpclient=ERROR
cp "$_reportDataDir/csv/*.csv" "$_newReportDir"
cd "$_reportsDir"
mkdir "archive" > /dev/null 2>&1
zip -r "archive/$_reportFile.zip" "$_newReportDir" > /dev/null 2>&1
cd "$_baseDir"
echo "Done."
